## Smooth transition to sections using anchor links
### How to use
Create an anchor link:
```
<a href="#pig">
    <button>to pig</button>
</a>
```
When you click on it there is a smooth transition to the section with ```id``` = ```pig``` at a speed of 300ms
```
<script>
    $('a').toTarget({
        speed: 300
    });
</script>
```
