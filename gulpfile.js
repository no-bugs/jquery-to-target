const minify = require('gulp-minify');
const gulp   = require("gulp");
gulp.task('compress', function() {
    return gulp.src('src/*.js')
        .pipe(minify())
        .pipe(gulp.dest('dist'))
});