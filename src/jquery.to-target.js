/*! jquery scrollTop v1.0.0 25.08.2019
 * 
 *
 * Copyright (c) 2019 No Bugs;
 * Licensed under the MIT license */
(function ( $ ) {
    $.fn.toTarget = function(options) {
        var $items = this;
        var _options = {
            'speed': 400
        };
        _options = $.extend({}, _options, options);
        $items.click(function (e) {
            e.preventDefault();
            var target = document.getElementById(this.hash.substr(1));
            if(target) {
                $('body,html').animate({
                    scrollTop: target.offsetTop
                }, _options.speed);
            }
            return false;
        });
    };
})(jQuery);
